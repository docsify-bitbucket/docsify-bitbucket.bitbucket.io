# docsify-bitbucket sample

## Overview

This is a sample for [docsify-bitbucket](https://docsify-bitbucket.github.io) plugin.

## HTML

The HTML code for this sample is just a few lines. There is no logo, favicon, title, repository URL or any other project specific value so you can use it as a skeleton without modification.

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="//unpkg.com/docsify/lib/themes/buble.css">
</head>
<body>
  <div id="app"></div>
  <script>
    window.$docsify = {
    }
  </script>
  <script src="//unpkg.com/docsify/lib/docsify.min.js"></script>
  <script src="//unpkg.com/docsify-copy-code"></script>
  <script src="//unpkg.com/docsify-bitbucket"></script>
</body>
</html>
```
